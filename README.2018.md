# My Install

## First Installs

* [Chrome]
* [Slack]
* [Homebrew]
* [Visual Studio Code]
* [Caffeine](http://lightheadsw.com/caffeine/)
* [Dropbox]

## Terminal Setup (Mac)

![Terminal picture](https://gitlab.com/brendan/next-mac/raw/master/Screenshot_2018-09-13_11.06.23.png)

### Oh my zsh

1. Install oh-my-zsh
1. Reload oh-my-zsh folder from here
1. Reload .zshrc from here
1. Run `sudo easy_install Pygments`

### Brew installs

```bash
brew install gpg git redis postgresql libiconv icu4c pkg-config cmake nodejs go openssl node npm yarn coreutils re2
brew install gnupg gnupg2
brew install bat diff-so-fancy
brew tap homebrew/cask-fonts
brew cask install font-hack-nerd-font
```

### NPM Globals

```bash
npm install -g yo generator-code
```

### Git configs

```bash
git config --global push.default current
git config --global core.pager "diff-so-fancy | less --tabs=4 -RFX"
git config --global alias.lg=log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
```

### Color & Style

Install `homebrew.terminal`

### Jessie Frazelle setup

```bash
mkdir ~/repos/opensource
cd ~/repos/opensource
git clone git@github.com:jessfraz/dotfiles.git jess-dotfiles
git clone git@github.com:jessfraz/dockerfiles.git jess-dockerfiles
```

### Variables (old)

```bash
JAVA_HOME=/System/Library/Java/JavaVirtualMachines/1.8.0.jdk/Contents/Home
export JAVA_HOME
export PATH=$PATH:$JAVA_HOME/binexport JAVA_HOME="$(/usr/libexec/java_home -v 1.8)"
export PATH=/Users/Brendan/Applications/terraform/:$PATH
alias ll='ls -lG'
alias meteors='meteor --settings settings.json'
source ~/.profile

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/Brendan/Applications/google-cloud-sdk/path.bash.inc' ]; then source '/Users/Brendan/Applications/google-cloud-sdk/pat$

# The next line enables shell command completion for gcloud.
if [ -f '/Users/Brendan/Applications/google-cloud-sdk/completion.bash.inc' ]; then source '/Users/Brendan/Applications/google-cloud-s$
```

## Other Apps

* [Docker]
* [F.lux]
* [Skype]
* [Zoom]
* [Insomina](https://insomnia.rest/)
* [Battle.net](http://battle.net)
* [Discord](https://discordapp.com/)
* [https://gitlab.com/jlenny/hncli](https://gitlab.com/jlenny/hncli)
* [ZJ's snippet tool](https://gitlab.com/zj/snippet)

### Maybes

#### Randoms

* [GIPHY Capture]
* [Bartender 3]

#### Programing

* [XCode]
* [WebStorm]
* [RVM]

RVM itself:

   `\curl -sSL https://get.rvm.io | bash`

Readlines support:

   `rvm pkg install readline --verify-downloads 1`
   `ruby_configure_flags=--with-readline-dir="$rvm_path/usr"`
   `rvm reinstall 2.3.1`
