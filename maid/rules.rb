def basename(path)
  File.basename(path, File.extname(path))
end

def dirname(path)
  File.dirname(path)
end

def generate_glob(path,exts)
  File.join( File.dirname(path), basename(path) ).concat(exts)
end

def mkdir_if_not_exists(path)
  path=File.expand_path(path)
  if not File.directory?(path)
    mkdir(path)
  end
  return path
end

def mv(file, path)
  move file, mkdir_if_not_exists(path)
end

download_types = {
   'application/xpinstall' => 'applications',
   'public.disk-image' => 'applications',
   'com.apple.application' => 'applications',
   'com.apple.installer-package-archive' => 'applications',
   'application/rdf+xml' => 'data',
   'text/turtle' => 'data',
   'com.apple.ical.ics.event' => 'events',
   'org.idpf.epub-container' => 'documents', # .epub
   'public.rtf' => 'documents', # .rtf
   'org.openxmlformats.wordprocessingml.document' => 'documents', #docx
   'com.apple.iwork.pages.sffpages' => 'documents', # .pages
   'public.spreadsheet' => 'documents', # .xlsx, .numbers
   'application/tex' => 'documents',
   'public.plain-text' => 'documents', # .txt
   'public.presentation' => 'documents', # .pptx, .keynote
   'com.microsoft.word.doc' => 'documents',
   'public.json' => 'snippets',
   'public.php-script' => 'snippets',
   'public.archive' => 'archives',
   'public.audio' => 'audio',
   'public.image' => 'images',
   'com.adobe.pdf' => 'pdf',
   'public.movie' => 'video',
   'public.html' => 'snippets',
}

download_types = download_types.each do |key,value|
  download_types[key] = "~/Downloads/_sorted/#{value}"
end

Maid.rules do

  rule 'ISOs, debs, dmgs, zips' do
    trash(dir('~/Downloads/*.iso'))
    trash(dir('~/Downloads/*.deb'))
    trash(dir('~/Downloads/*.dmg'))

    found = dir('~/Downloads/*.zip').select { |path|
      zipfile_contents(path).any? { |c| c.match(/\.app$/) }
    }

    trash(found)
  end

  rule 'Desktop Cleanup' do
    dir('~/Desktop/*').each do |path|
      unless path == '/Users/brendan/Desktop/Stuff'
        if 1.days.since?(accessed_at(path))
          move(path, '~/Desktop/Stuff/')
        end
      end
    end
  end

  rule 'Large old Downloads (larger than 5MB)' do
    dir('~/Downloads/*').each do |path|
      if File.size(expand(path)) > 5000000
        if 1.week.since?(accessed_at(path))
          trash(path)
        end
      end
    end
  end
  
  # NOTE: Currently, only Mac OS X supports `downloaded_from`.
  rule 'Old files downloaded while developing/testing' do
    dir('~/Downloads/*').each do |path|
      if downloaded_from(path).any? { |u| u.match('http://localhost') || u.match('http://staging.yourcompany.com') } &&
          1.week.since?(accessed_at(path))
        trash(path)
      end
    end
  end

end
